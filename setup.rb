#!/bin/ruby
require 'yaml'

def project_name(project)
  project.split('/').last.gsub(/.git/, '')
end

File.readlines('projects.txt').each do |project|

  config = {}

  config["before_script"] = [
    # "apt-get --quiet update --yes",
    # "apt-get --quiet install --yes git golang",
    "git clone #{project.chomp}",
    "cd #{project_name(project).chomp}"
  ]

  config["include"] = [
    { "template" => "Android-Fastlane.gitlab-ci.yml" }
  ]


  puts config.to_yaml

  File.write("#{project_name(project).chomp}-ci.yml", config.to_yaml)
end
